"use strict";
// defines that js code should be executed in
// "strict mode"
// with strict mode, you cannot use undeclared variables

//CONDITIONAL STATEMENTS

//if statements
//allows you to execute code  based on certain conditions

//syntax
// - code here will be executed if condition is true
// }

//example

var numberOfLives = 0;
if (numberOfLives === 0) {
    alert("Game over");
}

//if / else statements
// else statement - execute when condition
//in the if statement is false

//syntax
//if (condition) {
// code here will be executed if condition is true
// }
// else {
//code executed if condition in if statement is false

// example
var b = 1;

// if (b === 10) {
//     console.log(`b is 10`)
// }
// else {
//     console.log("b is NOT 10")
// }

//else if statement

if (b === 10) {
    console.log(`b is 10`)
}
else if ("b < 10") {
    console.log("b is less than 10")
}
else {
    console.log("b is NOT 10")
}

// SWITCH STATEMENTS
//less duplicated and it increases readability in code
// syntax:
//switch (condition) {
//casee // code that gets executed
// break; // stops the code when case is executed
// case ''
//break;
//..
//..
//default '' equivalent to the else statement
//break;
// }

//EX

var color = "red";
switch (color) {
    case "blue":
        console.log("you chose blue");
        break;
    case "red":
        console.log("you chose red");
        break;
    case "pink":
        console.log("you chose pink");
        break;
    case "green":
        console.log("you chose green");
        break;
    default:
        console.log("you didnt select the right color");
    break;

}

// using a switch case statement
// create a program for the weather
// case for sunny, cloudy, rain, snow defualt

var currentweather = "hail"
switch (currentweather) {
    case "hail":
        break;
    case "sunny":
        console.log("its sunny")
        break;
    case "cloudy":
        console.log("its cloudy")
        break;
    case "rain":
        console.log("its rainy")
    case "snowing":
        break;
    case "snow":
        console.log("it's currently sunny");
        break;



//TERNARY OPERATOR
        //shorthand way of creatubg if/else statements
        //only used when there are two choices




// syntax
// (if condition is true) ? run this code : otherwise run this code

// EX

var numberOfHives = 6;

        (numberOfHives === 0) ? console.log("game over"): console.log("still alive")


}

// if / else if/ else

var burgerPref = prompt("what kind of burger do you like?");

if (burgerPref === "bacon and cheese") {
    alert("bacon and cheese burgers are bomb");
}
else if (burgerPref === "chili") {
    alert("what a joe!")
}
else if (burgerPref === "cheese") {
    alert("plain but good")
}
else {
    alert("good burger")
}




