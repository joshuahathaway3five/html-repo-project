function count(input) {
    console.log(input.length);
}

count("hello");


function add(a, b) {
    console.log(a + b)
}

add(10,5)

function subtract(a, b) {
    console.log(a - b)
}

subtract(10,5)

function multiply(a, b) {
    console.log(a * b)
}
multiply(10, 5)

function divide(a, b) {
    console.log(a / b)
}
divide(50,10)

function remainder(a, b) {
    console.log(a % b)
}
remainder(10,5)

function square(a) {
    // return a ** 2;
    // ES6 JS
    return a * a;
}
console.log(square(9));

// BONUS
//ANONYMOUSE function - functioin without a name and stores it in a variable

//SYNTAX
// var nameofvariable = function (){
//... code inside of the functions body
// }

var input = prompt("enter a string","string");

(function () {
    alert(input.length)
})();


/**
 * TODO:
 * Write a function named isOdd(x).
 * This function should return a boolean value.
 **/

function isOdd(x) {
    return x % 2 !== 0;
}
console.log(isOdd(5));

/**
 * TODO:
 * Write a function named isEven(x).
 * This function should return a boolean value.
 **/

function isEven(x) {
    return x % 2 === 0;
}
console.log(isEven(6));

/**
 * TODO:
 * Write a function named isSame(input).
 * This will return the input as the same in value and datatype return true.
 **/

var num1 = 10
var num2 = 10
function isSame() {
    return num1 === num2;
}
console.log(isSame())

/**
 * TODO:
 * Write a function named isSeven(x).
 * This function should return a boolean value.
 **/

function isSeven(x) {
    return x === 7;
}
console.log(isSeven(7))

/**
 * TODO:
 * WriTe a function named addTwo(x).
 * This should return the output plus 2.
 **/

function addTwo(a, b) {
    return x + 2;
}
console.log(addTwo(9)); //11

/**
 * TODO:
 * Write a function named isMultipleOfTen(x).
 * This function should return a boolean value.
 **/

function isMultipleOfTen(x) {
    return x % 10 === 0;
}
console.log(isMultipleOfTen(70));


/**
 * TODO:
 * Write a function named isMultipleOftwo(x).
 * This function should return a boolean value.
 **/
function isMultipleOftwo(x) {
    return x % 2 === 0
}
console.log(isMultipleOftwo(8));

/**
 * TODO:
 * Write a function named isMultipleOftwoAndFour(x).
 * This function should return a boolean value.
 **/

function isMultipleOftwoAndFour(x) {
    return x % 4 === 0 && x % 2 === 0;
}
console.log(isMultipleOftwoAndFour(30))

/**
 * TODO:
 * Write a function named isTrue(). This function should take in any input and return true
 * if the input provided is exactly equal to `true` in value and data type.
 **/
var inbut = "true";
function isTrue(x) {
    return inbut === "true";
}
console.log(isTrue())

/**
 * TODO:
 * Write a function named isFalse(x). This function should take in a value and returns a true
 * if and only if the provided input is equal to false.
 **/

var False = 10;
function isFalse(x) {
    console.log(x !== False);
}
isFalse("10");

/**
 * TODO:
 * Write a function named isVowel(x).
 * This function should return a boolean value.
 **/

function isVowel(x){
    return x=== `a` || x === 'e' || x === `i` || x === 'o'|| x === `u`
}
console.log(isVowel('x')) // false
console.log(isVowel('a')) // true

/**
 * TODO:
 * Write a function named triple(x).
 * This will return an input times 3.
 **/

function triple(x) {
    return x * 3;

}
triple(2)

/**
 * TODO:
 * Write a function named QuadNum(x).
 * This will return a number times 4.
 **/

function QuadNum(x) {
   return x * 4;

}
QuadNum(2)

/**
 * TODO:
 * Write a function named modulus(a,b).
 * This will return the remainder when a / b.
 **/

function modulus(a, b) {
    console.log(a % b)
}
modulus(11,5)

/**
 * TODO:
 * Write a function named degreesToRadians(x).
 * This function should convert from degrees to radians
 **/

// function degreesToRadians(x) {
//     var pi = Math.PI
//     return x * ( pi / 180);
// }
// console.log(degreesToRadians(45))

function degreesToRadians(x) {
    return x * (Math.PI / 180)
}
console.log(degreesToRadians(45))
/**
 * TODO:
 * Write a function named absoluteValue(x).
 * This will return absolute value of a number.
 **/

// function absoluteValue(x) {
//    return x = Math.abs(5 + 5);
// }
// console.log(absoluteValue(absoluteValue(Math.abs())))

function absoluteValue(x) {
    return parseInt(x);
}
console.log(absoluteValue("10"));
console.log(absoluteValue("stephen"))
// NAN - not a number


/**
 * TODO:
 * Write a function named reverseString(x).
 * This will return a string in reverse order.
 **/

 function reverseString(x) {
     var splitstring = x.split("");
     var reverseArray = splitstring.reverse();
     var joinArray = reverseArray.join("");
     return joinArray;
 }
 console.log(reverseString("hello"))

// function reverseString(x) {
//     return x.reverse()
// }
// console.log(reverseString("hello"))
// console.log(reverseString("Codebound"))
