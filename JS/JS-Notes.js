// External JavasScript

// single line comment

/*
this
is
a
multi
line
comment
 */

//console.log(5+100);

//data types
//primitive types
//numbers, strings, boolean, undefined, null.... objects

// 99.9 is a number

// "99.9" is a string

// false is a boolean


//VARIABLES
// var, let, const

//const means it can not be changed

//DECLARING VARIABLES
//syntax: var nameofthevariable;
//syntax: var nameofthevariable = value;
//let nameofvariable = value;
//const nameofthevariable = value;

// Declared a variable named firstname
var firstname;
//assigned the value of "billy" to firstname
firstname = "Billy";
//called the variable name
console.log(firstname);
//alert is a pop-up box

// alert(firstname);

//var firstname = "Billy";

// OPERATORS
//Arithmetic
/*
+ addition
- subtraction
* multiply
/ divided
% modulus (aka remainder)
x letter

*/

console.log(15 % 4);
console.log(5 % 4);
console.log((1 + 2) * 4/5);
// PEMDAS

// LOGICAL OPERATORS
// && - AND
// || - OR
// ! - NOT
// return a boolean value when used with boolean values
// also used with non-boolean values

// TRUE STATEMENT && TRUE STATEMENT (true)
// TRUE && FALSE (false)
// F && F (true)
// F && T (false)

// OR ||
// T || F (true)
// T || T (true)
// F || F (false)
// F || T (true)

// ! NOT (the opposite)
// !T (false)
// !F (true)

//!!!!!!!T (false)


// =, ==, ===

// = -used to  assign value to a variable
var lastname = "smith";

// COMPARISON OPERATOR
// == - checks if the value is the same
// === - checkis if the value AND the datatype are the same
// var firstpet = "dog";
// var secondpet = "cat";
//
// console.log(firstpet === secondpet);
//
// var age = 12;
// var age2 = "12"
// console.log(age == age2); //true
// console.log(age === age2); //false

//!= -checks if the value is NOT the same
//!== -checks if the value AND data type is NOT the same

// console.log(age != age2); // flase because age = 12 AND age2 is also 12
// console.log(age !== age2) //true because age is a number while age 2 is a string

// <, >, <=, >=
const age = 21;

console.log(age >= 21 || age < 18); // true
//T || F
console.log(age <= 21 || age > 18); //true
// T || T
console.log(age < 21 && age > 18); //false


//CONCATENATION
const person = "Bruce";
const person_age = 40;

console.log("Hello my name is " + person + " and I am " + person_age  +" years old ");

// TEMPLATE STRINGS
console.log(`hello my name is ${person} and i am ${person_age} years old`)

// GET THE LEGTH OF A STRING
console.log(person.length); //5

const greeting = "hello world";
console.log(greeting.length); //12

//GET THE STRING IN UPPERCASE / LOWERCASE
console.log(greeting.toUpperCase()); // HELLO WORLD

console.log(greeting.toLowerCase()); // hello world

// SUBSTRING
console.log(greeting.substring(0,5)); //hello
console.log(greeting.substring(6,11)); //world (no !)
console.log(greeting.substring(1,5)); //ello

// FUNCTIONS
// -a reusable block of code that performs a specified task

// SYNTAX FOR FUNCTION
/*
* function nameofthefunction() {
* code you want the function to do
* }
* */

function sayHello() {
    alert("Hello!!!");
}

// CALLING A FUNCTION
// when we want it to run, we call it by its name with the ()

sayHello();

function addNumber(num1) {
    console.log(num1 + 2);
}

addNumber(21);

function message(name, age) {
    console.log(`hello ${name} you are ${age} years old`)
}

message("clark",50);