"use strict"
// a tool for handling asynchronous events

//a promise represents an event that might not yet have happened
//a promise will be in one of three states:
// - pending - the event has not yet to happen
// -resolved - the event has hap[pened successfully
// -rejected - the event has happenend and a error occurred

// example
// const mypromise = new Promise((resolve, reject) => {
//     if (Math.random() > 0.5) {
//         resolve();
//     }
//     else {
//         reject();
//     }
// });
//
// mypromise.then(() => console.log("promise resolved"));
//     mypromise.catch(() => console.log("promise rejected"));






    //a promise object has two used methods
// .then() - accepts a callback that will run when the promise is resolved

// .catch - accepts a callback that runs when the promise is






// const mypromise = new Promise((resolve, reject) => {
//    setTimeout(() => {
//
//     if (Math.random() > 0.5) {
//         resolve();
//     }
//     else {
//         reject();
//     }
//     }, 2000);
// });
//
// mypromise.then(() => console.log("promise resolved"));
// mypromise.catch(() => console.log("promise rejected"));



//write a promise inside a function

function makearequest() {

    const mypromise = new Promise((resolve, reject) => {
   setTimeout(() => {

    if (Math.random() > 0.5) {
        resolve("here is your data");
    }
    else {
        reject("you got rejected");
    }
    }, 2000);
});
}
const request = makearequest();
console.log(request); // pending promise

request.then(data => console.log("promise resolved"));
request.request(error => console.log("promise rejected"));




// use a fetch and the promises returned from it

const mypromise = fetch('https://api.github.com/users');
mypromise.then(response => console.log(response));
mypromise.catch(error => console.log(error));



.// - the return value of a promises callback than can itself be treated as a promise, which allows us to CHAIN promises together

//const myPromise = new Promise()

Promise.resolve('one').then((one) => {
    console.log(one);
    return 'two';

}).then((two) => {
    console.log(two);
    return 'three';

}).then((three) => {
    console.log(three)

});




const hispromise = fetch('https://api.github.com/users').then((response) =>{
    const user = response.json().then((users) => {
        users.forEach((user) => {

        }
    )

    }, ) } )