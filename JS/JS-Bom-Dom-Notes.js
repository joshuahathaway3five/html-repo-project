"use strict"

// BOM NOTES
// Browser Object Model

// Hierarchy of objects in the browser.
//can target and manipulate HTML elements with JavaScript

// Location object
// manipulate the location of a document
//get query string parameter(s)

//navigator object
//query the info and capabilites of the browser

//screen object
//gets the info and manage the web browsers screen

//History
//get info/manage the web browsers history

//Window
//Document Object Model (DOM)

// WINDOW OBJECT
//represents the JavaScript global object
// all variables and functions declared globally with the 'var' keyword become
// the properties of the window object

// alert ()
// confirm ()
// prompt()
//setTimeout()
// sets a timer and executes a callback function once the timer expires
//setInterval
//executes a callback funtion repeatedly with a fixed delay between each call



//setTimeout()
// basic Syntax:
//var timeoutID = setTimeout( CallBackFunction{, delay, arg1, arg2...})

//delay is in milliseconds
// delay will defualt to 0

// function timeoutEx() {
//     setTimeout(function () {
//         alert('hello charlie cohort')
//
//         window.location = "https://www.Yahoo.com";
//     }, 5000);
// }
//
// //call timeoutExt
// timeoutEx();




//set Interval()

//setInterval( callbackfunction(), delay, arg1, arg2, ...)

// function intervalEx() {
//
//     setInterval(function () {
//         alert("hello world")
//
//     }, 3000)
// }
//
// intervalEx();
// clearInterval(); //stops the setInterval();


// var count = 0;
// var max = 10;
// var interval = 2000;
//
//
// var intervalID = setInterval(function () {
//
//     if (count >= max) {
//         clearInterval(intervalID);
//         console.log("finished")
//     } else {
//         count++;
//         console.log(count);
//     }
//
// }, interval);







//DOM - document object model

//manipulate HTML using JavaScript


//locating elements:
// target them by :
//element (tag) ex <h1>
//class .
//id #

//getElement by

//basic syntax
// document.getElementBy('name of the element, class or id')

// var btn3clicked = document.getElementById('btn3');
// alert(btn3clicked);

// accessing form inputes
//we can access forms using the form collection

// var usernameinput = document.forms.login.username;
//console.log(usernameinput);

//accessing html elements using class

// var cards = document.getElementsByClassName('card')
// console.log(cards);

// //accessing html elements using tag
//  var sections = document.getElementsByTagName('section')
// console.log(sections);

//queryselector()
//returns the first element within that document

// var headertitle = document.querySelector('header h1')
//
// var maintitle = document.querySelector('#main-title')
//
// var cardselector = document.querySelectorAll( '.card');
// console.log(cardselector);
//
// console.log(headertitle)
// console.log(maintitle)

// ACCESSING / Modifying elements and properties

//get the value of innerHTML

// var title = document.getElementById('main-title');
// console.log(title); //get the structure of #main-title
//
// //only want the content of the element?
// console.log(title.innerHTML);
//
// console.log(document.getElementById('main-title').innerHTML);
//
// // set the value of the innerHTML
//
// title.innerHTML = "hello <em> Charlie </em>!";


// ACCESSING AND MODIFY using attributes

// check if the attribute exists

var checkform = document.forms['login'];
//
// console.log(checkform);
// console.log(checkform.hasAttribute('action'));
//true


//get an attribute value
// console.log(checkform.getAttribute("method"));
//
// // creat a new attr. or change a value of an existing
//
// checkform.setAttribute('id', 'feedback-form');
// checkform.setAttribute('method', 'GET');
// //changes method from post to get
//
// console.log(checkform);
//
// //delete attr.
// checkform.removeAttribute('id',);
//
// console.log(checkform);

//accessing and modifying styles

//single style

var jumbotron = document.querySelector('.jumbotron')
jumbotron.style.display = 'none';

//.jumbotron {
//  display: none
// }

jumbotron.style.fontFamily = "Comic Sans Ms";

// multiple styles

Object.assign(jumbotron.style,{
    border: "10 px solid blue",
    fontFamily: "Trajan",
    textDecoration: "underline"
});


//styling node list

var tableRows = document.getElementsByTagName("tr")

for (var i = 0; i < tableRows.length; i++) {

    if (i % 2 === 0 && i !== 0)
    tableRows[i].style.background = "skyblue";
}



var tablerowarray = Array.from(tableRows);

//now we can use a forEach.

tablerowarray.forEach(function (tableRow) {
     {
        tableRow.style.background = "skyblue"
    }
})

// adding and removing elements

//createElements
//removeChild
//appendChild
//replaceChild