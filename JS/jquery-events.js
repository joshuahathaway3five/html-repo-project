"use strict"
//MOUSE EVENTS
// .click() - event handler to the "click"
// .dbclick() "" double click
//.hover() - executed when the mouse pointer enters and leaves the element(s)

//MOUSE EVENT IN JS
//var eventElement = document.getElementById('#my-id');

// eventElement.addEventListener('click', function() {
// code to run...
// }


// MOUSE EVENT IN jquery

// syntax :
//$('selector').click(handler/function() );

// $('#charlie').click(function () {
//     alert("the h1 element with the id of charlie was clicked")

// });

// $("#charlie").dblclick(function () {
//     alert("the h1 element with the id of charlie was clicked")
// });


// .hover()
// $('selector').hover( handlerIn, handlerOut)

$("#charlie").hover(
    //handlerIn anonymous function
    function () {
        $(this).css('background-color', 'skyblue');
    },
    //handlerOut
    function () {
        $(this).css({'background-color': 'white', 'font-size': '24px'});
    }
);


//KEYBOARD EVENTS in JQuery

// Different types of keyboard events
//    .keydown()
//    .keypress()
//    .keyup()
//    .on()
//    .off()

//. keydown() - event handler to the "keydown"
//JS event / triggers that event on an element

 $('#my-form').keydown(function () {
     alert('blank')
 })


// .keypress()
//same as keydown, with one exception:
//Modifier keys (shift, control, esc....)
// -will trigger .keydown() but NOT keypress

//.keyup()
// syntax $('#my-form).keydown(function () {
//     alert('blank')
// });



// .on() .off() //RESEARCH THIS