"use strinct"

// FOR LOOP
//basic snytax:

// for (initialization; condition; increment;decrement) {
    //run the code
// }


//will run until condition is false

// for (var i = 100; i <=50; i--) {
//     console.log(i);
// }

// forEach
// basic syntax

//nameOfVariable.forEach(function(rating)){
//}

//syntax ofa fuction:

//function nameOfFunction(element, index, array) {
    //run the code
//}
//can only use forEach. on arrays

// for.Each seperates all the numbers from the array into thier own element but are still grouped.

var I = [100, 90, 80, 70];
I.forEach(function (i1) {
    console.log(i1);
});
