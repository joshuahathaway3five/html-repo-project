"use strict";

// EVENt LISTENERS
// ADDEVENTLISTENER()

    //basic syntax
    // target.addEventListener
// target - the object the event listener is registered// ty[e- type of event that is being listened for
//listener - the function that is called an event when a   event of types happen on target


//type ofevents

//type of events
//keyup
//keydown
//click
//change
//submit

var testButton = document.getElementById('testBtn')

// add event listener using anonymous function


testButton.addEventListener('click', function () {
    if (document.body.style.background === 'red') {
        document.body.style.background =  'white';
    } else {
        document.body.style.background = 'red';
    }

});

function togglebackground() {
    if (document.body.style.background === 'red') {
        document.body.style.background =  'white';
        testButton.removeEventListener('click', togglebackground)
    } else {
        document.body.style.background = 'red';
    }
}

testButton.addEventListener('click', togglebackground);



//    ADDITIONAL EVENTS

//cursor hovers over a paragraph, chnage the color of text, font-family, make font larger

var paragraph =  document.getElementsByTagName('p'[0])

//chagen font color
function makecolorchange() {

    paragraph.style.color = "green";
    paragraph.style.fontFamily = "Comic Sans Ms";
    paragraph.style.fontSize = "30px";
}

//add a mouse over event to the 'p' that wil;l trigger the makeColorChange ()
//function

paragraph.addEventListener("mouseover", function () {
        makecolorchange();
});