"use strict"

// MAP FILTER REDUCE
// all functions that operates on collections (arrays)
// all will NOT modify but will return a NEW COPY of the array

// .map - transform each element in the collection
// .filter - filters our values
//.reduce - reduces a collection to a single


//var numbers = [11, 20, 33, 40, 55, 60, 77, 80, 99, 100];

// var even = [];
// var odd = [];

// for (var i = 0; i < numbers.length; i++) {
//
//     // find all the even numbers
//     if (numbers[i] % 2 === 0) {
//
//         // add the index from the numbers to the evens
//         even.push(numbers[i]);
//     }
// }
//
// for (var x = 0; x < numbers.length; x++) {
//
//     // find all the even numbers
//     if (numbers[x] % 2 !== 0) {
//
//         // add the index from the numbers to the evens
//         odd.push(numbers[x]);
//     }
// }
//
// console.log(even);
// console.log(odd);

// var evens = numbers.filter(function (n) {
//     return n % 2 === 0;
// });
// console.log(evens);
//
// var odds = numbers.filter(function (n) {
//     return n % 2 !== 0;
// });
// console.log(evens);
// console.log(numbers)
//
// // map
//
// var increment = numbers.map(function (num) {
//     return num + 2;
// });
//
// console.log(increment);


// EXAMPLES ABOVE IN ES6
const numbers = [11, 20, 33, 40, 55, 60, 77, 80, 99, 100];
const evens = numbers.filter(n => n % 2 === 0);
console.log(evens)
const odds = numbers.filter(num => num % 2 !== 0);
console.log(odds);

const increment = numbers.map(num3 => num3 + 2);
console.log(increment)

// REDUCE
    // .reduce used to reduce a collection to a single value

// const nums = [1, 2, 3, 4, 5];
// const sum = nums.reduce((accumulation, currentNumber) => {
//     return accumulation + currentNumnber;
//
// }, 0)

const officeSales = [
    {name: 'Jim Halpert',
     sales: 500
    },
    {name: 'Dwight Schrute',
        sales: 500
    },
    {
        name: 'Ryan Howard',
        sales: 150
    }
];

const totalSales = officeSales.reduce((total, person) => {
    return total + person.sales;
}, 0);

console.log(totalSales);

// 0 is defualt unless with objects