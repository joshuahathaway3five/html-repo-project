"use strict"

// load users in our console log

$.ajax("https://jsonplaceholder.typicode.com/users",    {
    type: "get",

}).done(function (data) {
    console.log(data)

var dataHTML = displayusers(data);

    $("#users").html(dataHTML);
});


function displayusers(users) {

    var usersOnHTML = '';

    users.forEach(function (user) {

        usersOnHTML += `
        <div class="user">
            <h3>Employee Name: ${user.name}</h3>
            <a href="http://${user.website}"></a>
            <ul>
                 <li>
                      <P>Username: ${user.username}</P>
                </li>
                <li>
                      <p>Address: ${user.address.street}, ${user.address.city}</p>
                </li>
            </ul>
        </div>
        `
    });

    return usersOnHTML;
}