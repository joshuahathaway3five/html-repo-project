"use strict";

// OBJECTS

//objects is a grouping of data and functionality. data items inside of an object = properties
// functions insixe of an object = methods

//CUSTOm OBJECTS preototypes that allows existing objects to be used as templates to creat new objects
//Object() keyword - the starting point to make custome objects

//NEW OBJECT INSTANCE
// var car = new Object();
// console.log(typeof car);

//The use of `new object()` calls the Object
//CONSTRUCTOR top build a new INSTANCE of object.

// OBJECT LITERAL NOTATION
// - curly braces {}

// var car = {};
// alert(typeof car); //object

//SETTING UP PROPERTIES ON A CUSTOM OBJECT
// dot notation
// car.make = `Toyota`;
//
// // array notation
//
// car[`model`] = '4Runner';
//
// console.log(car);


//MOSST COMMON WAY TO ASSIGN PROPERTIES
// var car = {
//     make: `Toyota`,
//     model: `4Runner`,
//     color: `Hunter Green`,
//     numberOfWheels: 4
// };
//
// // console.log(car);
//
// console.log(car.model);
//
// //adding on more properties
// // Don't do this
// car[`numberOfDoors`] = 5;
//
// //INSTEAD
// car.numberOfDoors = 5;
// console.log(car);


//NESTED VALUES
var cars = [
    {
        make: 'Toyota',
        model: '4Runner',
        color: 'Hunter Green',
        numberOfWheels: 5,
        features: ["Heated Seats", "Bluetooth Radio", "Automatic Doors"],
        alarm: function () {
            alert("Sound the Alarm!");
        }
    },
    {
        make: 'Honda',
        model: 'Civic',
        color: 'Black',
        numberOfWheels: 4,
        features: ["Great Gas Mileage", "AM/FM Radio", "Still Runs"],
        alarm: function () {
            alert("No Alarm... sorry");
        },
        owner: {
            name: 'John Doe',
            age: 35
        }
    },
    {
        make: 'Nissan',
        model: 'Sentra',
        color: 'Red',
        numberOfWheels: 4,
        features: ["Great Gas Mileage", "AM/FM Radio", "Still Runs", "Power Windows", "GPS"],
        alarm: function () {
            alert("Call the cops!");
        },
        owner: {
            name: 'Jane Doe',
            age: 30,
            address: {
                street: '150 Alamo Plaza',
                city: 'San Antonio',
                state: 'TX',
                citySlogan: function () {
                    alert("Go Spurs Go!")
                }
            }
        }
    }
];

// console.log(cars);
// get the honda object

// console.log(cars[1]);
// get the model of the honda object only

// console.log(cars[1].model);
// get the owner of the nissan sentra only

// console.log(cars[2].owner);
// get the city of the nissan owner

// console.log(cars[2].owner.address.city);
// get the second feature of the honda civic only

// console.log(cars[1].features[1]);
// get the alarm function of the first object
// console.log(cars[0].alarm());
//
//
// // display the features of all cars
// cars.forEach(function (car) {
//
//     car.features.forEach(function (feature) {
//         console.log(feature);
//     })
//
// });

//`this` keyword

//this - is simply a reference to the current object
//can reffer to a different object based on how a function is being called.

//ex.

var videoGame = {};
videoGame.name = `Madden 20`;
videoGame.company = `EA Sports`;
videoGame.genre = `Sports`;

//create a method on the videoGame object

videoGame.description =
    function () {
        alert(`Video Game:` + this.name + `Company of Video Games:` + this.company + `Genre of Video Game:` + this.genre);
    };

videoGame.description();
