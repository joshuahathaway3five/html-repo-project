"use strict"

//Arrays
//data structure that holds on ordered list of items
//a variable that contains a list of values

/*
{} - braces
[}- brackets

Basic Syntax
[] empty array
['red']
['red, 'blue', 'white'] 3 elements inside this array
[4, 4.4, -4, 0] an array of numbers, 4 elements in this array
['red', 4, true, -4] a mixed array

*/

// var myArray = ['black', 12, -144, false];
// console.log(myArray);
// console.log(myArray.length); //4
// console.log(myArray[0]); // black
// console.log(myArray[4]);
//

//ITERATING ARRAY
//traverse through elements

var Charlie = ['josh', 'jose', 'nick', `karen`, 'stephen']
// console.log(Charlie)
//
// console.log('There are' + Charlie.length + 'members in the charlie cohert');


// LOOP THROUGH AN ARRAY AND CL THE VALUES

for (var i = 0; i < Charlie.length; i++) {
    console.log('the person at index' + i + 'is' + Charlie[i]);
}

//forEach loop
/*

Syntax:
nameOfVariable.forEach( function(element, index, array) {
    code to run
    });

    ex:
*/

var rating = [25, 34, 57, 62, 78, 89, 91];

// rating.forEach(function (rating) {
//     console.log(rating)
// })

var pizza = ['cheese', 'dough', 'sauce', 'pineapple', 'canadian bacon']
// pizza.forEach(function (ingrediants) {
//     console.log(ingrediants);
// })

//CHANGING / MANIPULATING ARRAYS
var fruits = ['banana', 'apple', 'grape', 'strawberry'];
console.log(fruits)

//adding to the array
//.push = add the end of the array
fruits.push('watermelon');
console.log(fruits);

//.unshift = add to the beggining of an array
fruits.unshift('dragon fruit');
console.log(fruits);
console.log(fruits[0]);

fruits.push('cherry', 'mango', 'pinapple')
console.log(fruits)

// REMOVE FROM ARRAY
// .pop removes lst element of an array
fruits.pop();
console.log(fruits)

// .shift() removes the first element

fruits.shift();
console.log(fruits);

var removedFruit = fruits.shift();
console.log('fruit removed' + removedFruit);


// LOCATING ARRAY ELEMENT(S)
console.log(fruits);

//.indexOf returns the first occurance of what your looking for and the index

var indexElement = fruits.indexOf('watermelon');
console.log(indexElement)

// SLICING
//.slice coeps a portion of the array returns a new array (does not copy the original)
var slice = fruits.slice(2, 5);
console.log(slice);
console.log(fruits);

//REVERSE

//.reverse will reverse the original array and returns the reversed array

fruits.reverse();
console.log(fruits);

//SORT
//.sort will SORT the original array and return the sorted array

console.log(fruits.sort());

//SPLIT
//.split takes a string and turns it into an array
var codebound = 'leslie, stephen, karen';
var codeboundArray = codebound.split(",")
console.log(codeboundArray)

// JOIN
//.join takes an array and converts it into a string with a delimiter of your choice

var newcodebound = codeboundArray.join(",");
console.log(newcodebound)