"use strict";

//Document Ready

window.onload = function () {
    alert("this page has finished loading")
};

//jquesry ogject
// object used to find and create HTML elements from the DOM

//syntax:
// $(document).ready(function() {
// run the code...
// });

$(document).ready(function () {
    alert("this page has finished loading")
})


//In Jquery use the doolar sign $ to refrence the jQuery object
// $ is an alias of jquery

//jQuery Selectors
//ID, class, Element, multiple, all
// #
// .
// elementtagname
// selector 1, selector 2
// *


//syntax for jquery selectors
// $("selector")

// .html - returns the html content(s) of the selected element(s)
//SIMILAR to 'innerHTML' property

// .css - allows us to change css properties for the selected element or element(s)
//SIMILAR to the '.style' property


// example
var content = $('#codebound').html();
alert(content);

$('.urgent').css('background-color', 'red'); // single property
$('.not-urgent').css({'background-color':'yellow', 'text-decoration': 'line-through'});
//multiple propertys

$('.urgent, p').css('color', 'orange');
$('.urgent, p').css('text-decoration', 'orange');
//multiple selector



$('*').css('background-color',);
//all selector

//element selector
$('h3').css('border-style', 'dotted');




//in a css
// .urgent {
// background-color: red;
// }