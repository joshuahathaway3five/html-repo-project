"use strict"
//JAVASCRIPT LOOPS
// allows us to execute code repeatedly

//while loop, do-while loop, for loop, and break and continue

// while loop
//is a basic looping construct that will execute a block of code, as long as a certain condition is true

/*

syntax:
while (condition) {
    //run the code
}


// example

var num = 0;
while (num <= 100) {
    console.log(`#` + num);

//increment a value by 1...
    num += 1;
        or
    num++;


 // do-while loop
 the only difference from a while loop is that the condition is evaluated at the end of the loop
 instead of the beginning

syntax
do {
///run the code
} while (condition)

 EX
 var I = 10;
 do {
    console.log("Do-while #" + I);
    I -= 1;
    or
    I--;
 } while (i <= 0)

 //var is 10 so it wont go down because (I <= 0) instead of (I >= 0)


 // For loop
 is a robust loop mechanism available in many programming languages.

 syntax

 for (initialization; condition; increment/decrement) {
        //run the code
 }


 //example
 for (var x = 100; x <= 200; x+=10) {
 console.log("for loop #" + x);
 }



//break ands continue
breaking out a loop
using the `break` keyword allows you to exit the loop
*/

var endAtNumber = 5;

for (var i = 1; i <= 100; i++) {
    console.log("loop count #" + i);

    if (i === endAtNumber) {
        alert("reached our limit. Break!");
        break;
        console.log("we broke");
        // you wont see this ^
    }
}

//continue

for (var n = 1; n <= 100; n++) {

    //if statement
    if(n % 2 !== 0) {
        continue;
    }

    console.log("even number" + n);
    // prints out n % 2 === 0
}


